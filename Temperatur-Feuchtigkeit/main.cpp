#include "mbed.h"
#include "sht15.hpp"

Serial pc(USBTX, USBRX);

// Der Sensor wird auf Port D9, D10 Gnd, VDD
SHTx::SHT15 sensor(D9, D10);

int
main() {
    sensor.setOTPReload(false);
    sensor.setResolution(true);

    while(1) {
        busy = true;
        sensor.update();
        busy = false;

        // Temperatur in Celsius
        sensor.setScale(false);
        pc.printf("Temperature [ %3.2f C ]\n", sensor.getTemperature());

        // Temperatur in Fahrenheit
        sensor.setScale(true);
        pc.printf("            [ %3.2f F ]\n", sensor.getTemperature());

        // Relatice Feuchtigkeit
        pc.printf("Humidity    [ %3.2f %% ]\n", sensor.getHumidity());

        wait(5);
    }
}